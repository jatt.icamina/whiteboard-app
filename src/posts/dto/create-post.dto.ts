import { IsEnum, IsNotEmpty } from 'class-validator';
import { PostFlair } from '../post-flair.enum';
export class CreatePostDto {
    @IsNotEmpty()
    headline: string;

    @IsNotEmpty()
    description: string;
    
    @IsEnum([PostFlair.BIRTHDAY, PostFlair.EVENT, PostFlair.GENERAL])
    flair: PostFlair;
}